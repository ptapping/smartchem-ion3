Welcome to smartchem-ion3's documentation!
==========================================

This is a python interface to the SMARTChem Ion3 ion-selective electrode instrument.

It requires the device to be connected to the host computer using a RS232 style serial port, such as
through a USB-to-serial converter, and then provides a basic wrapper around the device's control
capabilities. See the device's `user manual
<https://cdn.shopify.com/s/files/1/0552/9924/4191/files/smartCHEM_Ion3v1_0.pdf>`__ for details on
its capabilities.

Source code at `<https://gitlab.com/ptapping/smartchem-ion3>`__.

Documentation online at `<https://smartchem-ion3.readthedocs.io/>`__.


User Guide
----------

Installation
````````````

Install using pip:

.. code-block:: sh

  pip install --user --upgrade smartchem-ion3

Alternatively, download the code and install it manually. The only dependency is on the ``pyserial``
library.


Configuring the Instrument
``````````````````````````

It is recommended to configure the instrument to run at its fastest communication rate (38400 baud).
This can be done using the instrument's menus, ``Menu->F4->F4->F1->F4->Menu``. To instead run at a
slower speed, the baud rate can be configured using the ``baudrate`` parameter during the
initialisation of the :data:`~smartchem_ion3.Ion3` object.


Basic Usage
```````````

.. code-block:: python

  import smartchem_ion3

  # Make a function to pretty print data obtained from the device
  def handle_data(d):
      """Handle receipt of spontaneous data sent from the instrument."""
      print(f"Reading #{d.n} at {d.timestamp.isoformat()}")
      print(f"  Channel 1, {d.ch1.unit.description} = {d.ch1.value}{d.ch1.unit.suffix}")
      print(f"  Channel 2, {d.ch2.unit.description} = {d.ch2.value}{d.ch2.unit.suffix}")
      print(f"  Channel 3, {d.ch3.unit.description} = {d.ch3.value}{d.ch3.unit.suffix}")
      print(f"  Temperature, {d.cht.unit.description} = {d.cht.value}{d.cht.unit.suffix}")

  # With no parameters, will use the first detected serial port at 38400 baud.
  ion3 = smartchem_ion3.Ion3()
  
  # Get a reading of the current state of the data channels and print it out
  handle_data(ion3.current_data())

  # Get the logged data and print them out
  for d in ion3.logged_data():
    handle_data(d)


Receiving Spontaneous Readings
``````````````````````````````

The instrument can send back data readings spontaneously, when using its "Instant Send" or "Print"
modes. To receive these data messages, set a ``data_callback`` function either during the
:data:`~smartchem_ion3.Ion3` initialisation or by assigning to its
:data:`~smartchem_ion3.Ion3.data_callback` property.

.. code-block:: python

  import smartchem_ion3

  def handle_data(d):
      """Handle receipt of spontaneous data sent from the instrument."""
      print(f"Reading #{d.n} at {d.timestamp.isoformat()}")
      print(f"  Channel 1, {d.ch1.unit.description} = {d.ch1.value}{d.ch1.unit.suffix}")
      print(f"  Channel 2, {d.ch2.unit.description} = {d.ch2.value}{d.ch2.unit.suffix}")
      print(f"  Channel 3, {d.ch3.unit.description} = {d.ch3.value}{d.ch3.unit.suffix}")
      print(f"  Temperature, {d.cht.unit.description} = {d.cht.value}{d.cht.unit.suffix}")

  ion3 = smartchem_ion3.Ion3(data_callback=handle_data)


Selecting the Serial Port Device
````````````````````````````````

By default, the first discovered serial device will be used to connect to the instrument. To select
a more specific device, several parameters are available during initialisation of the
:data:`~smartchem_ion3.Ion3` object. The serial port name can be specified exactly (eg.
``"/dev/ttyUSB0"``) however, as that is subject to change depending on other devices connected to the
system, it is not recommended. Additional parameters are available to find the appropriate port.
These parameters are passed on to a helper function, :meth:`~smartchem_ion3.find_device` which
documents each option.

.. code-block:: python

  import smartchem_ion3

  # Select by a hardcoded port name (not recommended!)
  ion3 = smartchem_ion3.Ion3(serial_port="/dev/ttyUSB0")

  # Instead, use some combination of port information fields.
  # This helper lists port info available to filter on.
  print(smartchem_ion3.list_devices())
  # Select the port by the USB vendor and product ID numbers
  ion3 = smartchem_ion3.Ion3(vid=0x067b, pid=0x2303)


Known Issues
````````````

The device is fairly primitive and does not like to be controlled simultaneously through the serial
port and the front panel buttons. For example, if the "Print" button is pressed, the device will
send data over the serial port and may scramble any communications which might be occurring at the
time. Similarly, the device will not be able to communicate over the serial port when it is not on
it's default "home" screen. If any menu is open, communications will fail. Communications will also
not work while the device is displaying any status messages like the "Log #5 Recorded" message when
pressing the "Store" button.

If the device is used purely through the serial interface, there should be no problems, but if you
have issues connecting to the device, ensure it is not currently displaying any menus.


API Documentation
-----------------

.. toctree::
   :maxdepth: 5
   :titlesonly:

   api/smartchem_ion3


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
