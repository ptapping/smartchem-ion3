#!/usr/bin/env python3

import logging
logging.basicConfig(level=logging.DEBUG)

import smartchem_ion3

def handle_data(data):
    """Handle receipt of spontaneous data sent from the instrument."""
    print(f"Reading #{data.n} at {data.timestamp.isoformat()}")
    print(f"  Channel 1, {data.ch1.unit.description} = {data.ch1.value}{data.ch1.unit.suffix}")
    print(f"  Channel 2, {data.ch2.unit.description} = {data.ch2.value}{data.ch2.unit.suffix}")
    print(f"  Channel 3, {data.ch3.unit.description} = {data.ch3.value}{data.ch3.unit.suffix}")
    print(f"  Temperature, {data.cht.unit.description} = {data.cht.value}{data.cht.unit.suffix}")

# With no parameters, will use the first detected serial port at 38400 baud.
ion3 = smartchem_ion3.Ion3(data_callback=handle_data)
